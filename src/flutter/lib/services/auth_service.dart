import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/constants/api_path.dart';
import 'package:shop_app/services/session_storage_service.dart';

class AuthService {
  static AuthService? service;
  static String get ACCESS_TOKEN_KEY => "ACCESS_TOKEN";

  static Future<AuthService> getInstance() async {
    return service ??= AuthService();
  }

  Future<bool> auth(String username, String password) async {
    var res = await http.post(ApiPath.KEYCLOAK_AUTH, headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, body: {
      "username": username,
      "password": password,
      "client_id": ApiPath.KEYCLOAK_AUDIENCE,
      "grant_type": ApiPath.KEYCLOAK_GRANT_TYPE
    });

    if (res.statusCode == 200) {
      var sessionStorageService = await SessionStorageService.getInstance();
      var token = jsonDecode(res.body) as Map<String,dynamic>;
      var accessToken = token["access_token"];
      sessionStorageService?.save(ACCESS_TOKEN_KEY, accessToken);
      return true;
    }

    debugPrint(
        "An Error Occurred during loggin in. Status code: ${res.statusCode} , body: ${res.body.toString()}");
    return false;
  }
}
