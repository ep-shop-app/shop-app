import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/services/session_storage_service.dart';

// TODO: need request builder
class ApiService {
  static String get ACCESS_TOKEN_KEY => "ACCESS_TOKEN";

  late HttpClient _client;

  ApiService() {
    _client = HttpClient();
  }

  Future<HttpResponse<T?>> get<T>(
      Uri url, dynamic Function(Map<String, dynamic>) fromJson) async {
    final authHeaders = await _createAuthHeader();
    return _client.get(url, authHeaders, fromJson);
  }

  Future<HttpResponse<T?>> post<T>(Uri url, Object body,
      dynamic Function(Map<String, dynamic>)? fromJson) async {
    final authHeaders = await _createAuthHeader();
    return _client.post(url, body, authHeaders, fromJson);
  }

  Future<HttpResponse<T?>> put<T>(Uri url, Object body,
      dynamic Function(Map<String, dynamic>)? fromJson) async {
    final authHeaders = await _createAuthHeader();
    return _client.put(url, body, authHeaders, fromJson);
  }

  Future<HttpHeaders> _createAuthHeader() async {
    var sessionStorageService = await SessionStorageService.getInstance();
    var accessToken = sessionStorageService?.get(ACCESS_TOKEN_KEY);
    if (accessToken == null) {
      debugPrint("No access token in local storage found");
      throw Exception("No Auth");
    }
    return HttpHeaders(headers: {"Authorization": "Bearer $accessToken"});
  }
}

class HttpResponse<T> {
  final T? body;

  final HttpHeaders headers;

  final int status;

  final String? reasonPhrase;

  bool get ok => status == 200;

  HttpResponse(this.body, this.headers, this.status, this.reasonPhrase);
}

class HttpHeaders {
  Map<String, String> headers = {};

  HttpHeaders({required this.headers});

  bool has(String name) {
    return headers.containsKey(name);
  }

  String? get(String name) {
    return has(name) ? headers[name] : null;
  }
}

class HttpClient {
  Future<HttpResponse<T?>> get<T>(Uri url, HttpHeaders? headers,
      dynamic Function(Map<String, dynamic>) fromJson) async {
    var response = await http.get(url, headers: headers?.headers ?? {});
    return _extract<T>(response, fromJson);
  }

  Future<HttpResponse<T?>> post<T>(Uri url, Object body, HttpHeaders? headers,
      dynamic Function(Map<String, dynamic>)? fromJson) async {
    var response =
        await http.post(url, body: body, headers: headers?.headers ?? {});
    return _extract(response, fromJson);
  }

  Future<HttpResponse<T?>> put<T>(Uri url, Object body, HttpHeaders? headers,
      dynamic Function(Map<String, dynamic>)? fromJson) async {
    var response =
        await http.put(url, body: body, headers: headers?.headers ?? {});
    return _extract(response, fromJson);
  }

  HttpResponse<T?> _extract<T>(http.Response response,
      dynamic Function(Map<String, dynamic>)? fromJson) {
    T? body;

    if (response.statusCode == 200 && fromJson != null) {
      body = _deserialize(response.body, fromJson);
    }
    var headers = HttpHeaders(headers: response.headers);

    return HttpResponse(
        body, headers, response.statusCode, response.reasonPhrase);
  }

  T? _deserialize<T>(
      String json, dynamic Function(Map<String, dynamic> data) factory) {
    if (json.isNotEmpty) {
      var data = jsonDecode(json);

      if (data is List<dynamic>) {
        var t = data.map((e) => factory(e as Map<String, dynamic>)).toList()
            as T;
        return t;
      }

      return factory(data as Map<String, dynamic>);
    }
    return null;
  }
}
