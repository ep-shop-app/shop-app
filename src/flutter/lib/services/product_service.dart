import 'package:shop_app/models/product.dart';
import 'package:flutter/foundation.dart';
import 'package:shop_app/constants/api_path.dart';
import 'api_service.dart';
import 'dart:convert';

class ProductService extends ApiService {
  static ProductService? service;

  static ProductService getInstance() {
    return service ??= ProductService();
  }

  Future<List<Product>> getProducts() async {
    var result =
        await get<List<dynamic>>(ApiPath.PRODUCTS_API, Product.fromJson);

    if (result.status == 200) {
      return result.body?.map((e) => e as Product).toList() ?? [];
    }
    debugPrint(
        "An Error Occurred during get products. Url: ${ApiPath.PRODUCTS_API} Status code: ${result.status}, body: ${result.body.toString()}");
    return [];
  }

  Future<List<Product>> getPopularProducts() async {
    var result =
    await get<List<dynamic>>(ApiPath.POPULAR_PRODUCTS_API, Product.fromJson);

    if (result.status == 200) {
      return result.body?.map((e) => e as Product).toList() ?? [];
    }
    debugPrint(
        "An Error Occurred during get products. Url: ${ApiPath.POPULAR_PRODUCTS_API} Status code: ${result.status}, body: ${result.body.toString()}");
    return [];
  }
}
