import 'package:shared_preferences/shared_preferences.dart';

class SessionStorageService {
  static SessionStorageService? manager;
  static SharedPreferences? _prefs;

  static Future<SessionStorageService?> getInstance() async {
    if (manager == null || _prefs == null) {
      manager = SessionStorageService();
      _prefs = await SharedPreferences.getInstance();
    }
    return manager;
  }

  void save(String key, String value) {
    _prefs?.setString(key, value);
  }

  String? get(String key) {
    return _prefs?.getString(key);
  }

  void remove(String key) {
    _prefs?.remove(key);
  }

  Future clear() async {
    await _prefs?.clear();
  }
}
