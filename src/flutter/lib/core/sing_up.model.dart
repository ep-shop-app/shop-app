import 'package:json_annotation/json_annotation.dart';

part 'sing_up.model.g.dart';

@JsonSerializable()
class SingUp {
  String username;
  String email;
  String password;
  String confirmPassword;

  SingUp(
      {required this.username,
      required this.email,
      required this.password,
      required this.confirmPassword});

  factory SingUp.fromJson(Map<String, dynamic> json) => _$SingUpFromJson(json);

  Map<String, dynamic> toJson() => _$SingUpToJson(this);
}
