// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sing_up.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingUp _$SingUpFromJson(Map<String, dynamic> json) => SingUp(
      username: json['username'] as String,
      email: json['email'] as String,
      password: json['password'] as String,
      confirmPassword: json['confirmPassword'] as String,
    );

Map<String, dynamic> _$SingUpToJson(SingUp instance) => <String, dynamic>{
      'username': instance.username,
      'email': instance.email,
      'password': instance.password,
      'confirmPassword': instance.confirmPassword,
    };
