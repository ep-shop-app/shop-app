import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:shop_app/routes.dart';
import 'package:shop_app/screens/start/start_screen.dart';
import 'package:shop_app/theme.dart';

void main() {
  FlavorConfig(
    name: "DEV",
    // color: Colors.red,
    // location: BannerLocation.bottomStart,
    variables: {
      "BASE_API": "http://10.0.2.2:5001/api/",
      "KEYCLOAK_DOMAIN": "http://10.0.2.2:8080",
      "KEYCLOAK_REALM": "Demo",
      "KEYCLOAK_AUDIENCE": "mobile",
      "KEYCLOAK_GRANT_TYPE": "password"
    },
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: theme(),
      initialRoute: StartScreen.routeName,
      routes: routes,
    );
  }
}
