import 'package:flutter/material.dart';
import 'package:shop_app/components/product_card.dart';
import 'package:shop_app/models/product.dart';
import 'package:shop_app/services/product_service.dart';

import '../../../size_config.dart';
import '../../products_register/products_register.dart';
import 'section_title.dart';

class PopularProducts extends StatelessWidget {
  const PopularProducts({Key? key}) : super(key: key);

  Future<List<Product>> callAsyncFetch() async {
    var service = ProductService.getInstance();
    var items = await service.getProducts();

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: callAsyncFetch(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          }

          var products = snapshot.data as List<Product>;

          return Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: SectionTitle(
                    title: "Популярное",
                    press: () async {
                      var service = ProductService.getInstance();
                      var items = await service.getProducts();

                      ProductsRegisterScreen.products = items;

                      Navigator.pushNamed(
                          context, ProductsRegisterScreen.routeName);
                    }),
              ),
              SizedBox(height: getProportionateScreenWidth(20)),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    ...List.generate(
                      products.length,
                      (index) {
                        if (products[index].isPopular) {
                          return ProductCard(product: products[index]);
                        }

                        // here by default width and height is 0
                        return const SizedBox.shrink();
                      },
                    ),
                    SizedBox(width: getProportionateScreenWidth(20)),
                  ],
                ),
              )
            ],
          );
        });
  }
}
