import 'package:flutter/material.dart';
import 'package:shop_app/screens/start/start_screen.dart';
import 'package:shop_app/services/session_storage_service.dart';

import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          const ProfilePic(),
          const SizedBox(height: 20),
          ProfileMenu(
            text: "Мой аккаунт",
            icon: "assets/icons/User Icon.svg",
            press: () => {},
          ),
          ProfileMenu(
            text: "Настройки",
            icon: "assets/icons/Settings.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Служба поддержки",
            icon: "assets/icons/Question mark.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Выйти",
            icon: "assets/icons/Log out.svg",
            press: () async {
              var sessionStorageService =
                  await SessionStorageService.getInstance();
              await sessionStorageService?.clear();
              Navigator.pushNamedAndRemoveUntil(context, StartScreen.routeName,
                  (r) => !Navigator.canPop(context));
            },
          ),
        ],
      ),
    );
  }
}
