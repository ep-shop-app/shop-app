import 'package:flutter/material.dart';
import 'package:shop_app/components/product_card.dart';
import '../../../size_config.dart';
import '../products_register.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: getProportionateScreenWidth(20)),
      child: GridView.count(
        crossAxisCount: 2,
        mainAxisSpacing: 30,
        children:
            List.generate(ProductsRegisterScreen.products.length, (index) {
          return Center(
            child: ProductCard(product: ProductsRegisterScreen.products[index]),
          );
        }),
      ),
    );
  }
}
