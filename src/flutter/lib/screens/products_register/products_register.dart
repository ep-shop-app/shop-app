import 'package:flutter/material.dart';
import 'package:shop_app/models/product.dart';
import 'components/body.dart';

class ProductsRegisterScreen extends StatelessWidget {
  static String routeName = "/productsRegister";
  static List<Product> products = [];

  const ProductsRegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: buildAppBar(context), body: const Body());
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: const [
          Text(
            "Список товаров",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}
