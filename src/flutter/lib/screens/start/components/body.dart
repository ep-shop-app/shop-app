import 'package:flutter/cupertino.dart';
import 'package:shop_app/components/loader.dart';
import 'package:shop_app/screens/home/home_screen.dart';
import 'package:shop_app/screens/sign_in/sign_in_screen.dart';
import 'package:shop_app/services/session_storage_service.dart';

String get ACCESS_TOKEN_KEY => "ACCESS_TOKEN";

class Body extends StatefulWidget {
  @override
  _Body createState() => _Body();
}

class _Body extends State<Body> {
  bool? _loginUser;

  @override
  void initState() {
    super.initState();
    // Просто лоадер очень красивый))
    Future.delayed(const Duration(seconds: 1)).then((value) =>
        SessionStorageService.getInstance().then((sessionStorageService) {
          _loginUser = sessionStorageService?.get(ACCESS_TOKEN_KEY) != null;
          var route =
              _loginUser! ? HomeScreen.routeName : SignInScreen.routeName;
          Navigator.pushNamed(context, route);
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Center(child: CustomLoader());
  }
}
