import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shop_app/components/custom_suffix_icon.dart';
import 'package:shop_app/components/default_button.dart';
import 'package:shop_app/components/form_error.dart';
import 'package:shop_app/constants/api_path.dart';
import 'package:shop_app/core/user_info.dart';
import 'package:shop_app/models/user.dart';
import 'package:shop_app/screens/home/home_screen.dart';
import 'package:shop_app/services/api_service.dart';
import 'package:shop_app/services/session_storage_service.dart';

import '../../../constants/constants.dart';
import '../../../size_config.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? firstName;
  String? lastName;
  String? phoneNumber;
  String? address;

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          SizedBox(height: getProportionateScreenHeight(5)),
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(40)),
          buildLastNameFormField(),
          SizedBox(height: getProportionateScreenHeight(40)),
          buildPhoneNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultElevateButton(
            text: "Отправить",
            press: () async {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                var sessionStorageService =
                    await SessionStorageService.getInstance();

                var json = sessionStorageService?.get("USER_INFO");
                var userInfo = UserInfo.fromJson(jsonDecode(json!));

                var user = User(userInfo.username, userInfo.email, firstName!,
                    lastName!, phoneNumber!);

                var api = ApiService();
                var response = await api.put<void>(
                    Uri.parse(ApiPath.BASE_API + "users/" + user.username),
                    user.toJson(),
                    null);

                if (response.ok) {
                  Navigator.pushNamedAndRemoveUntil(context,
                      HomeScreen.routeName, (r) => !Navigator.canPop(context));
                } else {
                  addError(error: serverError);
                }
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPhoneNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: const InputDecoration(
        labelText: "Номер телефона",
        hintText: "Введите свой номер телефона",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSuffixIcon(svgIcon: "assets/icons/Phone.svg"),
      ),
    );
  }

  TextFormField buildLastNameFormField() {
    return TextFormField(
      onSaved: (newValue) => lastName = newValue,
      decoration: const InputDecoration(
        labelText: "Фамилия",
        hintText: "Введите вашу фамилию",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSuffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      onSaved: (newValue) => firstName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: const InputDecoration(
        labelText: "Имя",
        hintText: "Введите свое имя",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSuffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }
}
