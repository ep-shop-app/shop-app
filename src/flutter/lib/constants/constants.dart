import 'package:flutter/material.dart';
import 'package:shop_app/size_config.dart';

// TODO: разнести по файлам

const kPrimaryColor = Color(0xFF0D47A1);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String usernameAlreadyExist = "Аккаунт уже существует";
const String serverError = "Ошибка сервера";
const String kUsernameNullError = "Заполните имя пользователя";
const String kEmailNullError = "Заполните емайл";
const String kInvalidEmailError = "Неккоректный емайл";
const String kPassNullError = "Заполните пароль";
const String kShortPassError = "Пароль слишком короткий";
const String kMatchPassError = "Пароли не совпадают";
const String kNamelNullError = "Введите ваше имя";
const String kPhoneNumberNullError = "Введите номер телефона";
const String kAddressNullError = "Введите адресс";
const String wrongLoginOrPassword = "Неверный логин или пароль";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}
