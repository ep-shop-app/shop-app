import 'package:flutter_flavor/flutter_flavor.dart';

class ApiPath {
  static final String KEYCLOAK_DOMAIN =
      FlavorConfig.instance.variables["KEYCLOAK_DOMAIN"];

  static final String KEYCLOAK_REALM =
      FlavorConfig.instance.variables["KEYCLOAK_REALM"];

  static final String KEYCLOAK_AUDIENCE =
      FlavorConfig.instance.variables["KEYCLOAK_AUDIENCE"];

  static final String KEYCLOAK_GRANT_TYPE =
      FlavorConfig.instance.variables["KEYCLOAK_GRANT_TYPE"];

  static final Uri KEYCLOAK_AUTH = Uri.parse(
      '$KEYCLOAK_DOMAIN/auth/realms/$KEYCLOAK_REALM/protocol/openid-connect/token');

  static final BASE_API = FlavorConfig.instance.variables["BASE_API"];

  static final Uri PRODUCTS_API = Uri.parse(BASE_API + 'products');
  static final Uri POPULAR_PRODUCTS_API = Uri.parse(BASE_API + 'products?isPopular=true');
// static final Uri API_SECURED = Uri.parse('$baseApiUrl/secured');

// static final Uri API_NOT_SECURED = Uri.parse('$baseApiUrl/not-secured');
}
