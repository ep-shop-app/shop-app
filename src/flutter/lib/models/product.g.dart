// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      id: json['id'] as int,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      colors: _colorsFromJson(json['colors'] as List<dynamic>),
      rating: (json['rating'] as num?)?.toDouble() ?? 0.0,
      isFavourite: json['isFavourite'] as bool? ?? false,
      isPopular: json['isPopular'] as bool? ?? false,
      title: json['title'] as String,
      price: (json['price'] as num).toDouble(),
      description: json['description'] as String,
    );

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'images': instance.images,
      'colors': _colorsToJson(instance.colors),
      'rating': instance.rating,
      'price': instance.price,
      'isFavourite': instance.isFavourite,
      'isPopular': instance.isPopular,
    };
