import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

List<Color> _colorsFromJson(List<dynamic> json) => List<Color>.empty();
List<String> _colorsToJson(List<Color> colors) => [];

@JsonSerializable()
class Product {
  final int id;
  final String title, description;
  final List<String> images;

  // TODO: resolve
  @JsonKey(fromJson: _colorsFromJson, toJson: _colorsToJson)
  final List<Color> colors;
  final double rating, price;
  final bool isFavourite, isPopular;

  Product({
    required this.id,
    required this.images,
    required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description
  }) {
  }

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}

// Our demo Products

List<Product> demoProducts = [
  Product(
    id: 1,
    images: [
      "https://roscontrol.com/wp-content/uploads/2021/09/33b67e2f7ca1e10d3adb.jpg",
      "https://fruitsparadise.ru/wp-content/uploads/2021/03/sahar.jpg",
      "https://komus-opt.ru/upload/iblock/318/680943.jpg",
      "https://i.otzovik.com/objects/b/960000/951953.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Русский сахар",
    price: 780.99,
    description: "Самый настоящий русский сахар. Успейте купить. Кол-во ограничено",
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 2,
    images: [
      "https://moscow.shop.megafon.ru/images/goods/881/88121_p_20.png",
      "https://realnoevremya.ru/uploads/articles/4f/8c/6b453197660c45f5.jpg",
      "https://mobiltelefon.ru/photo/april15/25/yota_phone_2_01.jpg"
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Yota Phone",
    price: 12400,
    description: "Yota Phone - российский аналог айфона",
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 3,
    images: [
      "assets/images/glap.png",
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Gloves XC Omega - Polygon",
    price: 36.55,
    description: description,
    rating: 4.1,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 4,
    images: [
      "https://www.tdportal.ru/upload/iblock/669/669d81b97872e105be9f01f373faa38a.jpg",
      "https://svetocopy66.ru/templates/smartphone/images/2.png"
    ],
    colors: [
      Color(0xFFF6625E),
      Color(0xFF836DB8),
      Color(0xFFDECB9C),
      Colors.white,
    ],
    title: "Бумага офисная",
    price: 2300,
    description: "Бумага офисная А4, 80 г/м2, 500 л., марка С, SVETOCOPY CLASSIC, Россия",
    rating: 4.1,
    isFavourite: true,
  ),
];

const String description =
    "Wireless Controller for PS4™ gives you what you want in your gaming from over precision control your games to sharing …";
