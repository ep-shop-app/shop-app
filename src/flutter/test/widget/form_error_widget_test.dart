import 'package:flutter_test/flutter_test.dart';
import 'package:shop_app/components/form_error.dart';
import 'package:shop_app/size_config.dart';

void main() {
  testWidgets('Form error widget', (WidgetTester tester) async {

    SizeConfig.screenHeight = 520;
    SizeConfig.screenWidth = 340;

    var errors = List.of(['error_1', 'error_2']);
    await tester.pumpWidget(FormError(errors: errors));

    find.text('error_1');
    find.text('error_2');
  });
}