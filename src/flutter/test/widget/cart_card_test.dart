import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:shop_app/models/cart.dart';
import 'package:shop_app/models/product.dart';
import 'package:shop_app/screens/cart/components/cart_card.dart';
import 'package:shop_app/size_config.dart';

void main() {
  testWidgets('Cart card widget', (WidgetTester tester) async {

    SizeConfig.screenHeight = 520;
    SizeConfig.screenWidth = 340;

    var product = Product(id: 10, images: List.of(['assets/images/ps4_console_white_1.png']),
        colors: List.of([Color(0xFFF6625E)]), title: 'Bucket', price: 1000, description: 'The best bucket');
    var cart = Cart(product: product, numOfItem: 5);
    await tester.pumpWidget(CartCard(cart: cart));

    find.text('Bucket');
    find.text('The best bucket');
  });
}