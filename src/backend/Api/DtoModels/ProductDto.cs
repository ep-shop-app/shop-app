﻿namespace Api.DtoModels;

/// <summary>
/// Dto-модель товара.
/// </summary>
public class ProductDto
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public List<string> Images { get; set; } = new();
    public List<string> Colors { get; set; } = new();
    public double Rating { get; set; }
    public double Price { get; set; }
    public bool IsFavourite { get; set; }
    public bool IsPopular { get; set; }
}