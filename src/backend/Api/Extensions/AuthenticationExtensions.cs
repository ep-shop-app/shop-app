﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace Api.Extensions;

public static class AuthenticationExtensions
{
    public static AuthenticationBuilder AddKeycloakAuthentication(this IServiceCollection services,
        IConfiguration configuration)
    {
        return services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Authority =
                    $"{configuration["Keycloak:Domain"]}/auth/realms/{configuration["Keycloak:Realm"]}";
                options.Audience = configuration["Keycloak:Audience"];
                options.RequireHttpsMetadata = configuration.GetValue<bool>("Keycloak:RequireHttpsMetadata");
                
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = configuration.GetValue<bool>(configuration["Keycloak:ValidateIssuer"])
                };
                
                options.Events = new JwtBearerEvents()
                {
                    OnAuthenticationFailed = context =>
                    {
                        var payload = new
                        {
                            context.Request.Method,
                            context.Request.Path,
                            context.Request.Query,
                            context.Request.Headers
                        };
                        
                         Log.Error(context.Exception, "Authentication Failed {@Payload}", payload);
                         return Task.CompletedTask;
                    }
                };
            });
    }
}