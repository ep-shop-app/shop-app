using Api;
using Api.Controllers;
using Api.Extensions;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddKeycloakAuthentication(builder.Configuration);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// TODO: add keycloak client

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("ViewProducts", policy => policy.RequireRole("user"));
});

builder.Services.AddNpgsql<ShopContext>(builder.Configuration.GetConnectionString("ShopContext"));

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    // .Enrich.FromLogContext()
    // .Enrich.WithProperty("job", "backend")
    .WriteTo.Async(configure => configure.Console(theme: SystemConsoleTheme.Colored))
    // .WriteTo.Async(configure => configure.GrafanaLoki("http://localhost:3100"))
    .CreateLogger();

builder.Host.UseSerilog();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseHttpsRedirection();

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.MapControllers();

try
{
    Log.Information("Run host");
    app.Run();
    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}