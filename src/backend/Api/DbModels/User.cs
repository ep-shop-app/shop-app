﻿namespace Api.DbModels;

public class User
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Surname { get; set; }
    public string? Email { get; set; }
    public string? Username { get; set; }
    public string? Phone { get; set; }
    public DateTime? Birth { get; set; }
    public List<Role> Roles { get; set; } = new();
}