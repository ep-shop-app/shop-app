﻿namespace Api.DbModels;

public class Order
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int Discount { get; set; }
    public string? Address { get; set; }
    public int TotalCost { get; set; }
    public OrderType Type { get; set; }
    public OrderStatus Status { get; set; }
    public List<Product> Products { get; set; } = new();
}

public enum OrderStatus
{
    Active,
    Canceled,
    Completed
}

public enum OrderType
{
}