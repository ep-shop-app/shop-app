﻿namespace Api.DbModels;

public class Product
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public List<string> Colors { get; set; } = new();
    public List<FileModel> Images { get; set; } = new();
    public double Rating { get; set; }
    public double Price { get; set; }
    public bool IsPopular { get; set; }
}