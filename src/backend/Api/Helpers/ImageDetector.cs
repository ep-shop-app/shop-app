﻿namespace Api.Helpers;

// based on https://devblogs.microsoft.com/scripting/psimaging-part-1-test-image/
public class ImageDetector
{
    // https://en.wikipedia.org/wiki/List_of_file_signatures
    /* Bytes in c# have a range of 0 to 255 so each byte can be represented as
     * a two digit hex string. */
    private static readonly Dictionary<string, string[][]> SignatureTable = new Dictionary<string, string[][]>
    {
        {
            "jpeg",
            new[]
            {
                new[] { "FF", "D8", "FF", "DB" },
                new[] { "FF", "D8", "FF", "EE" },
                new[] { "FF", "D8", "FF", "E0", "00", "10", "4A", "46", "49", "46", "00", "01" }
            }
        },
        {
            "gif",
            new[]
            {
                new[] { "47", "49", "46", "38", "37", "61" },
                new[] { "47", "49", "46", "38", "39", "61" }
            }
        },
        {
            "png",
            new[]
            {
                new[] { "89", "50", "4E", "47", "0D", "0A", "1A", "0A" }
            }
        },
        {
            "bmp",
            new[]
            {
                new[] { "42", "4D" }
            }
        }
    };

    /// <summary>
    /// Takes a byte array and determines the image file type by
    /// comparing the first few bytes of the file to a list of known
    /// image file signatures.
    /// </summary>
    /// <param name="imageData">Byte array of the image data</param>
    /// <returns>ImageFormat corresponding to the image file format</returns>
    /// <exception cref="ArgumentException">Thrown if the image type can't be determined</exception>
    public static string? GetImageType(byte[] imageData)
    {
        foreach (var signatureEntry in SignatureTable)
        {
            foreach (var signature in signatureEntry.Value)
            {
                var isMatch = true;
                for (var i = 0; i < signature.Length; i++)
                {
                    var signatureByte = signature[i];

                    // ToString("X") gets the hex representation and pads it to always be length 2
                    var imageByte = imageData[i]
                        .ToString("X2");

                    if (signatureByte == imageByte)
                        continue;
                    isMatch = false;
                    break;
                }

                if (isMatch)
                {
                    return signatureEntry.Key;
                }
            }
        }

        return null;
    }
}