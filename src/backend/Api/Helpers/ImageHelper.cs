﻿namespace Api.Helpers;

public class ImageHelper
{
    public static async Task<MemoryStream> GetMemoryStreamAsync(string url)
    {
        var bytes = await GetBytesAsync(url);
        return new MemoryStream(bytes);
    }

    public static string GetImageName(string url)
    {
        var uri = new Uri(url);
        var fileName = Path.GetFileName(uri.LocalPath);
        return GetFileName(fileName);
    }

    public static string GetFileName(string fileName)
    {
        var extension = Path.GetExtension(fileName);
        return $"{Guid.NewGuid()}{extension}";
    }

    public static async Task<byte[]> GetBytesAsync(string url)
    {
        using var webClient = new System.Net.WebClient();
        try
        {
            return await webClient.DownloadDataTaskAsync(url);
        }
        catch
        {
            throw new Exception($"Произошла ошибка при скачивании изображения по адресу {url}");
        }
    }
}