﻿using Api.DbModels;
using Api.DtoModels;
using Api.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers;

[ApiController]
[Route("api/products")]
public class ProductsController : ControllerBase
{
    private readonly ShopContext _context;

    public ProductsController(ShopContext context)
    {
        _context = context;
    }

    /// <summary>
    /// Получение списка товаров.
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<List<ProductDto>>> Get()
    {
        return GetMockedProducts();
        
        var items = await _context.Product.Include(q => q.Images).ToListAsync();
        return items.Select(MapProductToProductDto).ToList();
    }

    /// <summary>
    /// Создание товара.
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> Post(ProductDto product)
    {
        await _context.Product.AddAsync(MapProductDtoToProduct(product));
        await _context.SaveChangesAsync();
        return Ok();
    }

    private ProductDto MapProductToProductDto(Product item) =>
        new()
        {
            Id = item.Id,
            Description = item.Description,
            Price = item.Price,
            Rating = item.Rating,
            Title = item.Title,
            IsPopular = item.IsPopular,
            Colors = item.Colors,
            // todo
            Images = item.Images.Select(_ => $"http://localhost:5001/api/files/{_.Id}").ToList()
        };

    private Product MapProductDtoToProduct(ProductDto item)
    {
        var images = item.Images.Select(_ => new
        {
            Url = _,
            Data = ImageHelper.GetBytesAsync(_).Result
        });

        return new Product
        {
            Id = item.Id,
            Description = item.Description,
            Price = item.Price,
            Rating = item.Rating,
            Title = item.Title,
            IsPopular = item.IsPopular,
            Colors = item.Colors,
            Images = images.Select(_ => new FileModel
            {
                Data = _.Data,
                Name = ImageHelper.GetImageName(_.Url),
                Extension = ImageHelper.GetFileName(_.Url).Split(".").LastOrDefault(),
                ContentType = ImageDetector.GetImageType(_.Data)
            }).ToList()
        };
    }

    private List<ProductDto> GetMockedProducts() => new()
    {
        new ProductDto
        {
            Id = 1,
            Title = "Русский сахар",
            Description = "Самый настоящий русский сахар. Успейте купить. Кол-во ограничено",
            Images = new List<string>
            {
                "https://roscontrol.com/wp-content/uploads/2021/09/33b67e2f7ca1e10d3adb.jpg",
                "https://fruitsparadise.ru/wp-content/uploads/2021/03/sahar.jpg",
                "https://komus-opt.ru/upload/iblock/318/680943.jpg",
                "https://i.otzovik.com/objects/b/960000/951953.png",
            },
            Price = 780.99,
            Rating = 200,
            IsFavourite = true,
            IsPopular = true
        },
        new ProductDto
        {
            Id = 2,
            Title = "Yota Phone",
            Description = "Yota Phone - российский аналог айфона",
            Images = new List<string>
            {
                "https://moscow.shop.megafon.ru/images/goods/881/88121_p_20.png",
                "https://realnoevremya.ru/uploads/articles/4f/8c/6b453197660c45f5.jpg",
                "https://mobiltelefon.ru/photo/april15/25/yota_phone_2_01.jpg"
            },
            Price = 12400,
            Rating = 80,
            IsFavourite = false,
            IsPopular = true
        },
        new ProductDto
        {
            Id = 3,
            Title = "Компьютер iRU",
            Description = "оссийская компания iRU сообщила о готовности к производству компьютеров на базе отечественного процессора Baikal-M. Он станет основой тонких клиентов, малоформатных настольных ПК (small form factor, SFF) и моноблоков, объединивших в одном корпусе монитор и системный блок. Напомним, что по итогам IV квартала 2020 г. iRU оказалась в тройке лидеров на российском рынке настольных ПК, обогнав Acer и Dell и пропустив вперед только HP и Lenovo.",
            Images = new List<string> { "https://filearchive.cnews.ru/img/news/2021/08/09/iru602.jpg" },
            Price = 240000,
            Rating = 1,
            IsFavourite = true,
            IsPopular = false
        },
        new ProductDto
        {
            Id = 4,
            Title = "Бумага офисная",
            Description = "Бумага офисная А4, 80 г/м2, 500 л., марка С, SVETOCOPY CLASSIC, Россия",
            Images = new List<string>
            {
                "https://www.tdportal.ru/upload/iblock/669/669d81b97872e105be9f01f373faa38a.jpg",
                "https://svetocopy66.ru/templates/smartphone/images/2.png"
            },
            Price = 2300,
            Rating = 24,
            IsFavourite = true,
            IsPopular = false
        }
    };
}