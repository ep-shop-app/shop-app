﻿using Api.DbModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers;

[ApiController]
[Route("api/files")]
public class FilesController : ControllerBase
{
    private readonly ShopContext _context;

    public FilesController(ShopContext context)
    {
        _context = context;
    }

    /// <summary>
    /// Получение файла по идентификатору.
    /// </summary>
    [HttpGet("{id:int}")]
    public async Task<IActionResult> Get(int id)
    {
        var file = await _context.File.FirstOrDefaultAsync(_ => _.Id == id);

        if (file is null)
        {
            return NotFound();
        }

        return File(file.Data, file.ContentType ?? string.Empty);
    }

    /// <summary>
    /// Добавление файла.
    /// </summary>
    [HttpPost]
    public async Task<IActionResult> Post(IFormFile file)
    {
        await using var stream = file.OpenReadStream();
        var bytes = new byte[file.Length];
        await stream.ReadAsync(bytes);

        var fileModel = new FileModel
        {
            Data = bytes,
            Extension = file.FileName?.Split(".").LastOrDefault(),
            Name = file.FileName,
            ContentType = file.ContentType
        };

        await _context.AddAsync(fileModel);
        await _context.SaveChangesAsync();

        return Ok();
    }
}