﻿using Api.DtoModels;
using Keycloak.Net;
using Keycloak.Net.Models.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("api/users")]
public class UsersController : ControllerBase
{
    private readonly KeycloakClient _keycloakClient;
    private readonly ILogger<UsersController> _logger;
    private readonly string _realm;

    public UsersController(IConfiguration configuration, ILogger<UsersController> logger)
    {
        _logger = logger;
        _realm = configuration["Keycloak:Realm"];
        // TODO: keycloak user admin
        _keycloakClient = new KeycloakClient(configuration["Keycloak:Domain"], "admin", "admin");
    }

    [AllowAnonymous]
    [HttpPost("registration")]
    public async Task<IActionResult> Registration([FromForm] RegistrationDto dto)
    {
        var user = new User
        {
            UserName = dto.Username,
            Email = dto.Email,
            Enabled = true,

            Credentials = new[]
            {
                new Credentials
                {
                    Type = "password",
                    Value = dto.Password
                }
            }
        };

        var result = await _keycloakClient.CreateUserAsync(_realm, user);

        return result ? Ok() : Problem();
    }

    [HttpPut("{username}")]
    public async Task<IActionResult> Put(string username, [FromForm] User user)
    {
        var newKeycloakUser = new User
        {
            UserName = user.UserName,
            Email = user.Email,
            Enabled = true,
            FirstName = user.FirstName,
            LastName = user.LastName
        };

        var keycloakUser = (await _keycloakClient.GetUsersAsync(_realm, username: user.UserName))
            .FirstOrDefault();

        if (keycloakUser is null)
        {
            return NotFound();
        }

        var result = await _keycloakClient.UpdateUserAsync(_realm, keycloakUser.Id, newKeycloakUser);

        return result ? Ok() : Problem();
    }
}