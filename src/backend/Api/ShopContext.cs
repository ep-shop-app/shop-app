﻿using Api.DbModels;
using Microsoft.EntityFrameworkCore;

#pragma warning disable CS8618

namespace Api;

public class ShopContext : DbContext
{
    public DbSet<FileModel> File { get; set; }
    public DbSet<User> User{ get; set; }
    public DbSet<Product> Product{ get; set; }
    public DbSet<Order> Order{ get; set; }
    public DbSet<Role> Role{ get; set; }

    public ShopContext(DbContextOptions<ShopContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>();
        modelBuilder.Entity<Product>();
        modelBuilder.Entity<Order>();
        modelBuilder.Entity<Role>();
        modelBuilder.Entity<FileModel>(builder => { builder.ToTable("File"); });

        base.OnModelCreating(modelBuilder);
    }
}